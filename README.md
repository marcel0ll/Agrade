# Agrade ![Is build passing](https://travis-ci.org/0tho/Agrade.svg?branch=agrade2)

Um software para visualização e controle de sua grade curricular.

Você pode usá-lo  [aqui](http://www.agrade.info), de seu computador ou aparelho móvel.

### Como contribuir

Se achar um bug ou tiver uma sugestão envie [aqui](https://github.com/0tho/Agrade/issues).
